Document: python3-flask-dance
Title: Debian python3-flask-dance Manual
Author: <insert document author here>
Abstract: This manual describes what python3-flask-dance is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python3-flask-dance/python3-flask-dance.sgml.gz

Format: postscript
Files: /usr/share/doc/python3-flask-dance/python3-flask-dance.ps.gz

Format: text
Files: /usr/share/doc/python3-flask-dance/python3-flask-dance.text.gz

Format: HTML
Index: /usr/share/doc/python3-flask-dance/html/index.html
Files: /usr/share/doc/python3-flask-dance/html/*.html
